(async () => {
    const bcrypt = require('bcryptjs');
    try {
        let text = 'ok'
     
        let hash = await bcrypt.hash(text, await bcrypt.genSalt(10))
        let hash2 = await bcrypt.hash(text, await bcrypt.genSalt(10))
        console.log(hash)
        console.log(hash2)

        let compare = await bcrypt.compare(text , hash)
        let compare2 = await bcrypt.compare(text , hash2)
        console.log(compare)
        console.log(compare2)

    } catch (error) {
        console.log(error.message)
    }
})()
