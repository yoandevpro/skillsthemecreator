
const express = require('express');
const mysql = require('mysql');
const app = express();
const bodyparser = require('body-parser')
const multer = require('multer');
var  upload  = multer ( {  dest : ' uploads / ' } ) 
require('dotenv').config()
//const session = require('express-session')
app.use(bodyparser.json());
var urlencodedparser = bodyparser.urlencoded({ extended: true });
app.use(urlencodedparser);





// Create connection
const db = mysql.createConnection({
    host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database:  process.env.DB_NAME
});


// Connection
db.connect((err) => {
    if(err){
        throw err;
    }
    console.log('MySql Connected...');
});



//moteur EJS 
app.set('views', './views');
app.set('view engine', 'ejs');

// app.get('/', (req , res) => {
//     res.sendFile('accueil',{root: __dirname})
// });
//accueil page
app.get('/', (req , res) => {
    res.render('accueil')
});
//contact page
app.get('/contact', (req , res) => {
    res.render('contact')
});

//creation Theme Exigence page
app.get('/createExigenceTheme', (req , res) => {
    res.render('ExigenceThemeUser')
});
//compte recruteur
app.get('/recruiter', (req , res) => {
    res.render('recruiter')
});

 //login page
 app.get('/connection', (req, res) => {
    
         res.render('connection');
     
 });
  
//  Select Techno
 app.get('/getTechno', (req, res) => {
    let sql = 'SELECT * FROM Techno';
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.send(results);
    });
});
// Select single Techno
app.get('/getTechno/:id', (req, res) => {
   let sql = `SELECT * FROM Techno WHERE id = ${req.params.id}`;
   let query = db.query(sql, (err, result) => {
       if(err) throw err;
       console.log(result);
       res.send('la techno en fonction de l\'id est bien récupéré');
   });
});
//creation compte recruteur page
app.get('/create_recruiter_account', (req , res) => {
    
    res.render('create_recruiter_account');
});
//envoie formulaire creation theme exigence 
app.post('/createExigenceTheme', function(req , res,){
    console.log(req.body);
    let array = [
        req.body.techno,
        req.body.niveau
    ];
    let array2 = [
        req.body.Themename
    ];
    let sql = "insert into  Exigence(Techno_id,niveau) values(?,?)";
    let sql2 = "insert into Theme (nom) values(?)";
    db.query(sql,array,function (err, results , fields){
        if (err){
            console.log('error first-requeste')
            throw err;
        }

        else {
        db.query(sql2,array2,function (err, results , fields){
            
            if (err) {
                console.log('error second-requeste')
                throw err;
            }else {
                res.send('tout c\'est bien passé',results)             
            }
          //  res.redirect(302, '/recruiter');
           
        });}
    });
    
 
        
    });
    //supprimer un theme ajouté
    
    app.post('/createExigenceTheme', function(req , res,){
        console.log(req.body);
        let array = [
            req.body.techno,
            req.body.niveau
        ];
       
        let sql = "DELETE FROM `Exigence` WHERE `Exigence`.`id` = ? AND `Exigence`.`Techno_id` = ?"
        
        db.query(sql,array,function (err, results , fields){
            if (err){
                console.log('error first-requeste')
                throw err;
            }
        });
         });

//envoie du formulaire creation compte recruteur
app.post('/', function(req , res,){
//      (async () => {
         let bcrypt = require('bcryptjs');
//          try {
//             let password = ''
//              let hash = await bcrypt.hash(req.body.password, await bcrypt.genSalt(10))
//              console.log(hash)
//              let compare = await bcrypt.compare(req.body.password , hash)
//              console.log(compare)
//          } catch (error) {
//              console.log(error.message)
//          }
//  })()

    console.log(req.body);
    let array = [
        req.body.nom ,
        req.body.prenom, 
        req.body.entreprise ,
        req.body.email ,
        bcrypt.hashSync(req.body.password, 10)
        
    ];
    let sql = "insert into RecruiterUser(nom,prenom,entreprise,email,password) values(?,?,?,?,?)";
    db.query(sql,array,function (err, row , fields){
        if (err) throw err
        
        res.redirect(302, '/connection');
        console.log(req.body.password, hash)
   
    });
        db.end();
    });

 app.listen(PORT=process.env.PORT || 4000, () => {
     console.log(`Server started on port 4000 `);
 });
// Insert Techno
// app.get('/addTechno', (req, res) => {
//     let sql = 'INSERT INTO Techno SET ?';
//     let query = db.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Techno added...');
//     });
//  });
// /*// Create DB
// app.get('/createdb', (req, res) => {
//     let sql = 'CREATE DATABASE nodemysql';
//     db.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Database created...');
//     });
// });

// // Create table
// app.get('/createtable', (req, res) => {
//     let sql = 'CREATE TABLE ExempleTable(id int AUTO_INCREMENT, title VARCHAR(255), body VARCHAR(255), PRIMARY KEY(id))';
//     db.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send(' table created...');
//     });
// });*/



// // Update Techno
// app.get('/updatepost/:id', (req, res) => {
//     let newTitle = 'Updated Techno';
//     let sql = `UPDATE Techno SET ? = '${newTitle}' WHERE id = ${req.params.id}`;
//     let query = db.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Techno updated...');
//     });
// });
// // Delete Techno
// app.get('/deleteTechno/:id', (req, res) => {
//     let sql = `DELETE FROM Techno WHERE id = ${req.params.id}`;
//     let query = db.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Techno deleted...');
//     });
// });


 //Ne pas lancer le server sans commenter les requete//
